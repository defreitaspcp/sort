/*
Author: Drozdek, A;
Modified by: de Freitas, P.C.P;
Description - Sort Algorithm;
Other sorting algorithms: Shell Sort, Heap Sort, Merge Sort, Radix Sort, Bit Radix Sort, Counting Sort, 
Comb Sort, Shell Sort,...;
References
[1]{Selection Sort, Bubble Sort, Insertion Sort, Quick Sort}, Drozdek, http://www.mathcs.duq.edu/drozdek/DSinCpp/sorts.h
*/
#include<iostream>
#include<cstdio>
#include<vector>
using namespace std;
class Sort
{
public:
	Sort();
	~Sort();
	void selectionsort(int data[], const int n) 
	{
		for (int i = 0, least, j; i < n - 1; i++) 
		{
			for (j = i + 1, least = i; j < n; j++)
			{
				if (data[j] < data[least])
				{
					least = j;
				}
			}
			swap(data,least,i);
		}
		display(data, n);
	}
	void bubblesort(int data[], const int n) 
	{
		for (int i = 0; i < n - 1; i++)
		{
			for (int j = n - 1; j > i; --j)
			{
				if (data[j] < data[j - 1])
				{
					swap(data, j, j - 1);
				}
			}
		}
		display(data, n);
	}
	void insertionsort(int data[], const int n) 
	{
		for (int i = 1, j; i < n; i++)
		{
			int tmp = data[i];
			for (j = i; j > 0 && tmp < data[j - 1]; j--)
			{
				data[j] = data[j - 1];
			}
			data[j] = tmp;
		}
		display(data, n);
	}
	void quicksort(int data[], const int n)
	{
		int i, max;
		if (n < 2)
		{
			return;
		}
		for (i = 1, max = 0; i < n; i++)// find the largest
		{
			if (data[max] < data[i])// element and put it
			{
				max = i;                // at the end of data[];
			}
		}
		swap(data, n-1, max); // largest el is now in its
		quicksort(data, 0, n - 2);     // final position;
		display(data, n);
	}
private:
	void display(int vec[], int size)
	{
		for (int i = 0; i < size; i++)
		{
			printf("%i ", vec[i]);
		}
		printf("\n");
	}
	void swap(int data[], int i, int j)
	{
		int temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}
	void quicksort(int data[], int first, int last) 
	{
		int lower = first + 1, upper = last;
		swap(data, first, (first + last) / 2);
		int bound = data[first];
		while (lower <= upper) 
		{
			while (data[lower] < bound)
			{
				lower++;
			}
			while (bound < data[upper])
			{
				upper--;
			}
			if (lower < upper)
			{
				swap(data, lower++,upper--);
			}
			else
			{
				lower++;
			}
		}
		swap(data,upper,first);
		if (first < upper - 1)
		{
			quicksort(data, first, upper - 1);
		}
		if (upper + 1 < last)
		{
			quicksort(data, upper + 1, last);
		}
	}
};

Sort::Sort()
{
}

Sort::~Sort()
{
}
int main()
{
	Sort sort;
	int v2[4] = { 777, 88, 9, 55 };
	//sort.selectionsort(v2, 4);
	//sort.bubblesort(v2, 4);
	//sort.insertionsort(v2, 4);
	sort.quicksort(v2, 4);
	while (true)
	{

	}
	return 0;
}