##Sort Algorithm[6]

Um algoritmo de ordena��o � um algoritmo que coloca elementos de uma lista em uma determinada ordem.

Algorithms: Selection Sort[2], Bubble Sort[3], Insertion Sort[4], Quick Sort[5].

Other sorting algorithms: Shell Sort, Heap Sort, Merge Sort, Radix Sort, Bit Radix Sort, Counting Sort, Comb Sort, Shell Sort, ...;

##References

1. [1]{Selection Sort, Bubble Sort, Insertion Sort, Quick Sort}, Drozdek, http://www.mathcs.duq.edu/drozdek/DSinCpp/sorts.h
2. [2]Selection Sort, wikipedia, https://en.wikipedia.org/wiki/Selection_sort
3. [3]Bubble Sort, wikipedia, https://en.wikipedia.org/wiki/Bubble_sort
4. [4]Insertion Sort, wikipedia, https://en.wikipedia.org/wiki/Insertion_sort
5. [5]Quick Sort, wikipedia, https://en.wikipedia.org/wiki/Quicksort
6. [6]Sorting Algorithm, wikipedia, https://en.wikipedia.org/wiki/Sorting_algorithm